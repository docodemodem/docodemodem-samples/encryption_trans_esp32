/*
 * Sample program for DocodeModem
 * transmit with encrypt
 * Copyright (c) 2020 Circuit Desgin,Inc
 * Released under the MIT license
 */
#include <docodemo.h>
#include <SlrModem.h>
#include <AES.h>
#include <MD5.h>
#include "crc16.h"

#ifdef USE_HARD_CRYPT
#include <hwcrypto/aes.h>
esp_aes_context ctx;
#endif

#define SEND_PERIOD_MS 3000

const uint8_t CHANNEL = 0x10;   //10進で16チャネルです。通信相手と異なると通信できません。
const uint8_t DEVICE_DI = 0x00; //通信相手のIDです。0は全てに届きます。
const uint8_t DEVICE_EI = 0x1;  // 自分のIDです
const uint8_t DEVICE_GI = 0x02; //グループIDです。通信相手と異なると通信できません。

DOCODEMO Dm = DOCODEMO();
SlrModem modem;
HardwareSerial UartModem(MODEM_UART_NO);

//暗号化関連
AES aes;
char password[] = "Password"; //プロジェクトごとに変更してください。
byte aes_key[16];
byte aes_iv[16];
byte salt[8];
static MD5_CTX ctxMD5;

void rand_text(int length, byte *result)
{
  int i;
  char char_set[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!#$%&()=-~^@{[;+:*]},<.>/?";
  for (i = 0; i < length; i++)
  {
    result[i] = char_set[rand() % sizeof(char_set)];
  }
}

static int pkcs7_padding(uint8_t *src, int len)
{
  uint8_t slot = len % 16;
  uint8_t count = len / 16;
  uint8_t pad;

  slot == 0 ? (pad = 16) : (pad = 16 - slot);

  src += len;

  for (int i = 0; i < pad; i++)
  {
    *(src + i) = pad;
  }

  return 16 + count * 16;
}

void setup()
{
  Dm.begin(); //初期化が必要です。

  //デバッグ用シリアルの初期化です
  SerialDebug.begin(115200);
  while (!SerialDebug)
    ;

  //モデム用シリアルの初期化です。通信速度とポート番号を注意してください。
  UartModem.begin(MLR_BAUDRATE, SERIAL_8N1, MODEM_UART_RX_PORT, MODEM_UART_TX_PORT);
  while (!UartModem)
    ;

  //モデムの電源を入れて少し待ちます
  Dm.ModemPowerCtrl(ON);
  delay(150);

  //モデム操作用に初期化します
  modem.Init(UartModem, nullptr);

  //各無線設定を行います。電源入り切りするようであればtrueにして内蔵Flashに保存するようにしてください。
  modem.SetMode(SlrModemMode::LoRaCmd, false);
  modem.SetChannel(CHANNEL, false);
  modem.SetDestinationID(DEVICE_DI, false);
  modem.SetEquipmentID(DEVICE_EI, false);
  modem.SetGroupID(DEVICE_GI, false);

  //暗号化のためパスワードからkeyを作成します
  MD5::MD5Init(&ctxMD5);
  MD5::MD5Update(&ctxMD5, password, strlen(password));
  MD5::MD5Final(aes_key, &ctxMD5);

#ifdef USE_HARD_CRYPT
  esp_aes_init(&ctx);
  esp_aes_setkey(&ctx, aes_key, 128);
#endif
}

int count = 0;

void loop()
{
  uint8_t trans_buf[128+8];
  uint8_t buf[128];

  while (1)
  {
    //saltは毎回ランダムで作ります。
    rand_text(8, salt);
    memcpy(&trans_buf[0],salt,8);//saltは毎回頭につけて送ります。
    
    //暗号化のため、saltとパスワードからaes_ivを作成します
    MD5::MD5Init(&ctxMD5);
    MD5::MD5Update(&ctxMD5, password, strlen(password));
    MD5::MD5Update(&ctxMD5, salt, sizeof(salt));
    MD5::MD5Final(aes_iv, &ctxMD5);

    //送信データ作成。下記の場合23バイト長のASCIIですが、Binaryでも構いません。
    //データの種類を区別するためにヘッダを付けます。先頭のTはパケットの種類、次の２バイトに自身のIDを設定してみました。
    int size = sprintf((char *)buf, "T%02X%020d", DEVICE_EI, count++);
#if 1
      SerialDebug.write(buf, size);
      SerialDebug.println();
#endif

    //AESだけではデータエラーを検出できないのでCRC16を追加します
    uint16_t crc = crc16(0,buf,size);
    buf[size++] = (crc >> 8) & 0xff;
    buf[size++] = crc & 0xff;

    //16バイト単位にするため補間します
    int cipherlength = pkcs7_padding(buf, size);

    //AES128 cbc で暗号化します
#ifdef USE_HARD_CRYPT
    esp_aes_crypt_cbc(&ctx, ESP_AES_ENCRYPT, cipherlength, aes_iv, (uint8_t *)buf, (uint8_t *)&trans_buf[8]);
#else
    aes.do_aes_encrypt((byte *)buf, cipherlength, (byte *)&trans_buf[8], aes_key, sizeof(aes_key), aes_iv);
#endif

    int translength = cipherlength + 8; //salt分を足します

    //@DT04****\r\nを実行し、送信結果を戻します。@DT04と\r\nは自動で付加されます。
    auto rc = modem.TransmitData(trans_buf, translength);
    if (rc == SlrModemError::Ok)
    {
      //送信完了
      SerialDebug.printf("Send Ok len=%d:\r\n", translength);
#if 1
      for (int i = 0; i < translength; i++)
      {
        SerialDebug.printf("%02X", trans_buf[i]);
      }
      SerialDebug.println();
#endif
    }
    else
    {
      //キャリアセンスによって送信できなかったことを示します
      SerialDebug.println("Send Ng...");
    }

    delay(SEND_PERIOD_MS); //LoRaだと通信速度が遅いので間隔をあける
  }
}